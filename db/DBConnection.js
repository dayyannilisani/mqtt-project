const Sequelize = require('sequelize')
const config = require('config')
const sequelize = new Sequelize(config.get('datebase.name'), config.get('datebase.user'), config.get('datebase.password'), {
  host: config.get('datebase.host'),
  dialect: 'postgres',
  logging: false
});

module.exports = sequelize