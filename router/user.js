const express = require("express");
const router = express.Router();
const Errors = require("../models/Errors");
const User = require('../models/User')
const joi = require("@hapi/joi");
const bcrypt = require('bcrypt')
const uuid = require('uuid').v4
const jwt = require('jsonwebtoken')
const config = require('config')
const validTime = 60 * 60 * 24 * 30 * 12 * 10

router.post("/user", async (req, res, next) => {
  const schema = joi.object({
    email: joi.string().email({
      tlds: {
        allow: false
      }
    }).required(),
    password: joi.string().min(8).required(),
    private_key: joi.string().required()
  });
  const validation = schema.validate(req.body)
  if (validation.error) {
    throw Errors.JoiLoginError
  }
  req.body.id = uuid();
  req.body.password = await bcrypt.hash(req.body.password, 10)
  req.body.account_bought = Date.now()
  await User.create(req.body)
  const token = jwt.sign({
      email: req.body.email,
      privateKey: req.body.private_key
    },
    config.get("privateKey"), {
      expiresIn: validTime
    })


  return res.send({
    token: token
  })
});


router.post('/auth', async function (req, res) {
  const schema = joi.object({
    email: joi.string().email({
      tlds: {
        allow: false
      }
    }).required(),
    password: joi.string().min(8).required()
  });
  const validation = schema.validate(req.body)
  if (validation.error) {
    throw Errors.JoiLoginError
  }
  const user = await User.findOne({
    where: {
      email: req.body.email
    }
  });
  if (!user)
    throw Errors.InvalidUsername
  const passCheck = await bcrypt.compare(req.body.password, user.password)
  if (!passCheck)
    throw Errors.InvalidUsername
  const token = jwt.sign({
      id: user.id
    },
    config.get("privateKey"), {
      expiresIn: validTime
    })
  return res.send({
    token: token
  })
})

module.exports = router;