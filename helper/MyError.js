const { use } = require("express/lib/router");

class MyError {
  constructor(error, code, userMessage) {
    this.error = error.message;
    this.code = code;
    this.userMessage = userMessage
  }
}
module.exports = MyError;
