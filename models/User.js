const { DataTypes } = require('sequelize')
const sequelize = require('../db/DBConnection')

const User = sequelize.define('user',{
   id:{
       type:DataTypes.STRING,
       allowNull:false,
       primaryKey:true
   },
   email:{
       type:DataTypes.STRING,
       allowNull:false,
       unique:true,
       validate:{
           isEmail:true
       }
   },
   password:{
       type:DataTypes.STRING,
       allowNull:false
   },
   account_bought:{
       type:DataTypes.BIGINT
   },
   valid_days:{
       type:DataTypes.INTEGER,
       defaultValue:30 
   },
   private_key:{
       type: DataTypes.STRING,
       allowNull:false
   }
},{
    updatedAt: false,
    tableName:'users'
});

module.exports = User