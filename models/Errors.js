const MyError = require("../helper/MyError");

module.exports.JoiLoginError = new MyError(Error("invalid params"), 400,"لطفا ورودی های خود را به درستی وارد کنید")
module.exports.InvalidUsername = new MyError(Error("wrong username and password"),400,"نام کاربری و رمز عبور وارد شده اشتباه است")
module.exports.expiredUser = new MyError(Error("user has been expired"),400,"مهلت بسته شما به پایان رسیده")