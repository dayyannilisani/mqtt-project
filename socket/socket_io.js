const {
  app
} = require("../startup/routes");
const io = require("socket.io")(app);
const jwt = require("jsonwebtoken");
const config = require("config");
const aedes = require("aedes")();
const fs = require("fs");
const {
  v4
} = require("uuid");
var amqp = require("amqplib/callback_api");

const mqttDebug = require("debug")("application:mqtt");
const socketDebug = require("debug")("application:socket");
const rabbitMQDebug = require("debug")("application:rabbitmq");

const port = process.argv[3] || config.get("mqttPort");
const mqttClients = [];
const nodeID = v4();

let rabbitMQChannel;
const queue = "rabbitmqt";

amqp.connect("amqp://localhost", function (error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }
    rabbitMQChannel = channel;

    channel.assertQueue(queue, {
      durable: false,
    });

    // channel.sendToQueue(queue, Buffer.from(msg));
    // console.log(" [x] Sent %s", msg);

    channel.consume(
      queue,
      function (msg) {
        const body = JSON.parse(msg.content.toString());
        if (body.nodeID == nodeID) return;
        rabbitMQDebug(" [x] Received %s", body.message);
        body.myType = "rabbitmq";
        aedes.publish({
            topic: body.topic,
            payload: JSON.stringify(body),
          },
          (error) => {}
        );

        io.sockets.emit(body.topic, JSON.stringify(body));
      }, {
        noAck: true,
      }
    );
  });
});

aedes.on("clientReady", (client) => {
  const clientId = client.id.split("|||")[1];
  mqttDebug(`Client ${clientId} connected to mqtt`);
  // client.subscriptions = []
  // mqttClients.push(client)
  // console.log('number of connected mqtt clients', mqttClients.length)
});
aedes.on("clientDisconnect", (client) => {
  mqttDebug(`Client disconnected from mqtt`);
  // try {
  //     mqttClients.splice(mqttClients.indexOf(client), 1)
  // } catch (e) {}
  // console.log('number of connected mqtt clients', mqttClients.length)
});
aedes.on("subscribe", (subscriptions, client) => {
  let {
    id
  } = client;
  id = id.split("|||")[0];
  try {
    const {
      id: allowedTopic
    } = jwt.verify(id, config.get("privateKey"));
    let allowed = true;
    for (sub of subscriptions) {
      if (!sub.topic.startsWith(allowedTopic)) {
        allowed = false;
      }
    }
    if (!allowed) {
      mqttDebug("client should be disconnected");
      client.close();
    } else {
      // const index = mqttClients.indexOf(client)
      // for (sub of subscriptions) {
      // mqttClients[index].subscriptions.push(sub.topic.toString())
      // }
      // console.log(`number of subscription for client is ${mqttClients[index].subscriptions.length}`)
    }
  } catch (e) {
    mqttDebug("client should be disconnected");
    mqttDebug(e.message);
    client.close();
  }
});

aedes.on("publish", (packet, client) => {
  if (packet.topic.startsWith("$SYS")) return;

  const payload = JSON.parse(packet.payload.toString());
  mqttDebug(payload.message);

  if (payload.myType != "mqtt") return;

  payload.topic = packet.topic + payload.subTopic;
  io.sockets.emit(payload.topic, JSON.stringify(payload));

  payload.nodeID = nodeID;
  rabbitMQChannel.sendToQueue(queue, Buffer.from(JSON.stringify(payload)));
});

// socket.io configuration
// todo : complete the comments
io.on("connection", (socket) => {
  socketDebug("a new connection");
  socket.on("message", (msg) => {
    try {
      socketDebug(msg.message);
      const body = jwt.verify(msg.token, config.get("privateKey"));
      socket.broadcast.emit(body.id + msg.subTopic, JSON.stringify(msg));

      if (msg.myType != "socket") return;

      msg.topic = body.id + msg.subTopic;
      aedes.publish({
          topic: msg.topic,
          payload: JSON.stringify(msg),
        },
        (error) => {}
      );

      msg.nodeID = nodeID;
      rabbitMQChannel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
    } catch (e) {
      socketDebug(e.message);
      socket.send({
        error: {
          code: 400,
          message: "invalid token",
          userMessage: "توکن ارسالی معتبر نمی باشد",
        },
      });
    }
  });
});

let server;
if (config.get("sslForMqtt")) {
  const options = {
    key: fs.readFileSync("D:/Codes/mqtt/key.pem"),
    cert: fs.readFileSync("D:/Codes/mqtt/certificate.pem"),
  };

  server = require("tls").createServer(options, aedes.handle);
  mqttDebug("Mqtt is Secured");
} else {
  server = require("net").createServer(aedes.handle);
  mqttDebug("Mqtt is not Secured");
}

server.listen(port, function () {
  console.log("Mqtt started and listening on port ", port);
});

module.exports.io = io;