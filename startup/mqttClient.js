const mqtt = require("mqtt");
const base64 = require("base64url");
const {
    v4
} = require('uuid')
const config = require('config')
const fs = require('fs')
const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1OGY2ZTVmLWQ3MmQtNGY2ZC1hMjE0LTVmNTdlZTIxNWRmMiIsInByaXZhdGVLZXkiOiJ0ZXN0aW5nIiwiaWF0IjoxNTk3ODk2OTc4LCJleHAiOjE2MDA0NDc2OTd9.n6eqG60hfkXxjJbqYaIIVozqmTU6DH5HhFkzMGdl-Ew";
const id = token + "|||" + v4()
let client

if (config.get("sslForMqtt")) {

    var options = {
        clientId: id,
        protocol: 'mqtts',
        clean: true,
        rejectUnauthorized: false,
        host: "localhost",
        port: process.argv[2] || config.get('mqttPort'),
        key: fs.readFileSync("D:/Codes/mqtt/startup/key.pem"),
        cert: fs.readFileSync("D:/Codes/mqtt/startup/certificate.pem"),
    }

    client = mqtt.connect(options)
} else {

    client = mqtt.connect("tcp://localhost:1883", {
        clientId: id,
        resubscribe: false,
    });

}
client.on("connect", (packet) => {
    console.log(`i connected to server 1883`);
    client.subscribe(mainTopic)
    client.publish(mainTopic, JSON.stringify({
        message: "hello",
        subTopic: "",
        myType: "mqtt",
        token
    }))
});
client.on('message', function (topic, message) {
    console.log(JSON.parse(message.toString()).message)
})
const mainTopic = JSON.parse(base64.decode(token.split(".")[1])).id;