const express = require("express");
require("express-async-errors");
const helmet = require("helmet");
const log = require("../middleware/log");
const auth = require("../middleware/auth");
const error = require("../middleware/error");
const user = require("../router/user");
const app = express();
const http = require('http').createServer(app);
module.exports.startup = function () {
  app.use(express.json());
  app.use(helmet());
  app.use(log);
  app.use(auth);
  app.use("/", user);
  app.get('/', (req, res) => {
    res.sendFile( __dirname + "/chat.html");
  });
  app.use(error);
};
module.exports.app = http