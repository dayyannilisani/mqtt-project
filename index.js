const express = require("express");
const config = require("config");
const sequelize = require('./db/DBConnection');
const user = require('./models/User')
const {
  app,
  startup
} = require('./startup/routes')
const socket = require('./socket/socket_io')
startup()

const port = process.argv[2] || config.get("port")
app.listen(port, () => {
  console.log(`server is listening on ${port}`);
  testDB()
});

async function testDB() {
  try {
    await sequelize.sync({
      force: true
    })
    console.log("connected to database")
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}