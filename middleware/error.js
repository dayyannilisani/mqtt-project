module.exports = function (err, req, res, next) {
  console.log(err)
  console.log(err.message)
  if(err.userMessage)
    res.status(err.code).send(err)
  else{
    if(err.original && err.original.constraint && err.original.constraint == "users_email_key"){
      res.status(400).send({
        code:400,
        message:"duplicate email",
        userMessage:"ایمیل استفاده شده تکراری است"
      })
    }
    else if(err.message && err.message == "invalid signature"){
      res.status(400).send({
        code:400,
        message:"invalid token",
        userMessage:"توکن ارسالی معتبر نمی باشد"
      })
    }
    else{
      res.status(500).send("internal server error")
    }
  }
};
