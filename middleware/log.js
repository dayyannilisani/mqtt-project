const log = require("debug")("app:");

module.exports = function logResponseBody(req, res, next) {
  var oldWrite = res.write,
    oldEnd = res.end;

  var chunks = [];

  res.write = function (chunk) {
    chunks.push(chunk);

    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk) chunks.push(chunk);
    try {
      var body = Buffer.concat(chunks).toString("utf8");
      log(`url = ${req.url}`);
      log(`method = ${req.method}`);
      log(`request body = ${JSON.stringify(req.body)}`);
      log(`response body = ${body}`);
    } catch (err) {}

    oldEnd.apply(res, arguments);
  };

  next();
};
